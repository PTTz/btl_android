

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.RigidBody) rb : cc.RigidBody;
    @property(cc.Node) bt_start : cc.Node;
    @property(cc.Node) bt_save : cc.Node;
    @property(cc.Node) bt_retry : cc.Node;
    @property(cc.Node) endBoard : cc.Node;
    @property(cc.Node) silver : cc.Node;
    @property(cc.Node) golden : cc.Node;
    @property(cc.Label) Score_Label : cc.Label;
    @property(cc.Label) Score_Label2 : cc.Label;
    @property(cc.Label) dataName : cc.Label;
    @property(cc.Label) dataScore : cc.Label;
    @property(cc.EditBox) eb_name : cc.EditBox;
    @property(cc.Node) node_eb_name : cc.Node;
    @property(cc.Node) whiteFlash : cc.Node;
    @property(cc.Node) tap2fly : cc.Node;

    @property(cc.AudioClip) soundFly : cc.AudioClip;
    @property(cc.AudioClip) soundDeath : cc.AudioClip;
    @property(cc.AudioClip) soundPointUp : cc.AudioClip;
    @property(cc.AudioClip) soundFalled : cc.AudioClip;
    @property(cc.AudioClip) soundSelect : cc.AudioClip;

    iddata : number;
    
    score : number = 0;
    isPlay = false;
    fallTimeout : any;
    falltween = cc.tween(this.node);
    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
        this.loadhightScore();
        this.score = 0;
        // cc.sys.localStorage.clear();// <-----------------------------------------
    }

    start () {
        this.rb.type = cc.RigidBodyType.Static;
    }
    pressStart(){
        this.rb.type = cc.RigidBodyType.Dynamic;
        this.node.getComponent(cc.PolygonCollider).enabled = true;
        this.isPlay = true;
        this.bt_start.active = false; 
        cc.audioEngine.playEffect(this.soundSelect,false);
        this.tap2fly.active = true;
        this.flyUp();
    }
    flyUp(){
        clearTimeout(this.fallTimeout);
        this.falltween.stop();
        if(this.isPlay){
            this.rb.linearVelocity = cc.v2(0,270);
            cc.audioEngine.playEffect(this.soundFly,false);
            cc.tween(this.node)
                .to(0.1, {angle : 10})
                .start();
        }
        this.fallTimeout = setTimeout(() => {
            this.falltween = cc.tween(this.node);
            this.falltween = this.falltween.to(1,{angle : -90});
            this.falltween.start();
        }, 400);
    }
    onCollisionEnter(other, self){
        if(other.tag == 1){ // dead
            this.tap2fly.active = false;
            this.whiteFlash.active = true;
            setTimeout(() => {
                this.whiteFlash.active = false;
            }, 30);
            this.node.getComponent(cc.PolygonCollider).enabled = false;
            this.isPlay = false;
            cc.tween(this.endBoard)
                .to(0.5,{position : cc.v3(0,112,0)})
                .start();
            cc.audioEngine.playEffect(this.soundDeath,false);
            this.saveScore();
        }

    }
    onCollisionExit(other, self){
        if(other.tag == 5 && this.isPlay){
            this.score += 1;
            this.Score_Label.string = (this.score.toString());
            this.Score_Label2.string = (this.score.toString());
            cc.audioEngine.playEffect(this.soundPointUp,false);
        }
    }

    onClickRetry()
    {
        cc.tween(this.endBoard)
        .to(0.5,{position : cc.v3(0,615,0)})
        .start();
        clearTimeout(this.fallTimeout);
        this.falltween.stop();
        this.node.angle = 0;
        this.node.setPosition;
        this.isPlay = false;
        this.node.setPosition(114,330);
        this.rb.type = cc.RigidBodyType.Static;
        this.score = 0;
        this.Score_Label.string = (this.score.toString());
        this.Score_Label2.string = (this.score.toString());
        this.bt_start.active = true; 
        // this.node.getComponent(cc.PolygonCollider).enabled = true;
    }
    entername(){
        cc.sys.localStorage.setItem("name",this.eb_name.string);
        this.bt_retry.active = true;
        this.bt_save.active  = false;
        this.loadhightScore();
        this.node_eb_name.active = false;
    }

    saveScore()
    {
        if((!cc.sys.localStorage.getItem("score")) || cc.sys.localStorage.getItem("score") < this.score){
            cc.sys.localStorage.setItem( "score" , this.score );
            this.silver.active = false;
            this.golden.active = true;
            this.bt_save.active = true;
            this.node_eb_name.active = true;
            this.bt_retry.active = false;
            this.loadhightScore();
        }
        else{
            this.silver.active = true;
            this.golden.active = false;
        }
    }
    loadhightScore(){
        var name = cc.sys.localStorage.getItem("name");
        var score = cc.sys.localStorage.getItem("score");
        if(name != null && score != null){
            this.dataName.string = name.toString();
            this.dataScore.string = score.toString();
        }

    }
    onClickSave(){
        
        cc.audioEngine.playEffect(this.soundSelect,false);
    } 
    getRandomInt(min, max) : number{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; 
    }

    // update (dt) {}
}
