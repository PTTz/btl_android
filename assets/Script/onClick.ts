

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    

    @property(cc.AudioClip) soundSelected : cc.AudioClip;
    @property(cc.Node) btPlay : cc.Node;
    // onLoad () {}
    onclickActive()
    {
        this.node.active = true;
        cc.audioEngine.playEffect(this.soundSelected,false);
        this.btPlay.active = false;
    }
    onClickDeactive()
    {
        cc.audioEngine.playEffect(this.soundSelected,false);
        this.node.active = false;
        this.btPlay.active = true;
    }
    start () {

    }

    // update (dt) {}
}
