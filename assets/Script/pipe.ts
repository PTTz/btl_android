
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.RigidBody) rbPipe : cc.RigidBody;
    @property(cc.Node) topPipe : cc.Node;
    @property(cc.Node) botPipe : cc.Node;
    @property(cc.Node) pipeInsideNode : cc.Node;
    @property(cc.RigidBody) rbPipeInside : cc.RigidBody;
    timeOut : any;
    speed : number;
    speedinside : number;
    isMove = false;
    tooHight : number;
    tooLow : number;

    onLoad () {
        cc.director.getPhysicsManager().enabled = true; 
        cc.director.getCollisionManager().enabled = true;
    }

    onCollisionStay(other, self){
        if(other.tag == 123 && this.node.active)
        {
            clearTimeout(this.timeOut);
            this.node.destroy();
        }
    }
    getSpeed( speed : number , speedInside : number ){
        this.speed = speed;
        this.speedinside = speedInside;
    }
    getRandomInt(min, max) : number{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; 
    }
    start () {
        var up = this.getRandomInt(0,1);
        if(up == 0)
        {
            this.rbPipe.linearVelocity = cc.v2(this.speed,-this.speedinside);
        } else {
            this.rbPipe.linearVelocity = cc.v2(this.speed,this.speedinside);
        }
        this.timeOut = setTimeout(() => {
            if(this.node.active){
                this.node.destroy();
            }
        }, 30000);
        this.tooHight = this.getRandomInt(100,230);
        this.tooLow = this.getRandomInt(-10,100);
    }
    update (dt) {
        if(this.node.position.y >= 230){
            this.rbPipe.linearVelocity = cc.v2(this.speed,-this.speedinside);
        }
        if(this.node.position.y <= -10){
            this.rbPipe.linearVelocity = cc.v2(this.speed,this.speedinside);
        }
    }
}
