import pipe from "./pipe"
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property([cc.Prefab]) pipe : [] = [];
    @property(cc.Node) pipeHere : cc.Node;
    @property(cc.Node) scoreList : cc.Node;
    @property(cc.Node) clearPipe : cc.Node;
    @property(cc.Node) pipeRoot : cc.Node;
    speed : number = -100;
    speedInside : number = 0;

    spawn : any;
    // @property(cc.Node) sound_select : cc.Node;
    @property(cc.AudioClip) soundSelect : cc.AudioClip;
    // @property(cc.AudioClip) theme : cc.AudioClip;

    isStart = false;
    delay : number = 4;
    pressStart()
    {
        this.isStart = true;
        this.spawn = setInterval(() => this.spawnpipe() , this.delay * 1000);
        cc.audioEngine.playEffect(this.soundSelect,false);
    }
    onClickRetry(){
        cc.audioEngine.playEffect(this.soundSelect,false);
        clearInterval(this.spawn);
        this.clearPipe.active = true;
        console.log(this.clearPipe.active);
        setTimeout(() => {
            this.clearPipe.active = false;
        }, 200);
        this.speed = -100;
        this.speedInside = 0;
    }
    spawnpipe()
    {
        var whichpipe = this.getRandomInt(0,2);
        var newpipe = cc.instantiate(this.pipe[whichpipe]);
        var y = this.getRandomInt(-40,260);
        if(whichpipe == 1){
            y = this.getRandomInt(-30,200);
        }
        newpipe.setPosition(500,y);
        newpipe.parent = this.pipeRoot;
        var pipeScript = newpipe.getComponent(pipe);
        pipeScript.getSpeed(this.speed, this.speedInside);
        if(this.speed <= 250){this.speed -= 5;}  
        if(this.speedInside <= 150){this.speedInside += 5;} 
    }

    getRandomInt(min, max) : number{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; 
    }
    onClickPause(){
        cc.director.pause();  
        clearInterval(this.spawn);
    }
    onCLickResume(){
        cc.director.resume();
        this.spawn = setInterval(() => this.spawnpipe() , this.delay * 1000);
    }
    onLoad () {

    }

    start () {

    }

    update (dt) {
    }
}
