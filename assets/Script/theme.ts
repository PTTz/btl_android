
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.AudioClip) themeSound : cc.AudioClip;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.audioEngine.playEffect(this.themeSound, true);
    }
    onDisable(){
        cc.audioEngine.stopAll;
    }
    start () {

    }

    // update (dt) {}
}
